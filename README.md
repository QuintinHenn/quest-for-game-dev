# Quest for Game Dev: So You Want to Be a Hero

```text
   ____                  _      __              _____                        _____
  / __ \                | |    / _|            / ____|                      |  __ \
 | |  | |_   _  ___  ___| |_  | |_ ___  _ __  | |  __  __ _ _ __ ___   ___  | |  | | _____   __
 | |  | | | | |/ _ \/ __| __| |  _/ _ \| '__| | | |_ |/ _` | '_ ` _ \ / _ \ | |  | |/ _ \ \ / /
 | |__| | |_| |  __/\__ \ |_  | || (_) | |    | |__| | (_| | | | | | |  __/ | |__| |  __/\ V /
  \___\_\\__,_|\___||___/\__| |_| \___/|_|     \_____|\__,_|_| |_| |_|\___| |_____/ \___| \_/

```

Learn to develop games by developing a text adventure, and progresseively increase the difficulty and scope to develop a block breaker, platformer and RPG game.

Start here with :earth_africa: [Quest for Game Dev](https://quintinhenn.gitlab.io/quest-for-game-dev/) on the Interweb :rocket:

## Getting Started

These instructions will get you a copy of the website project, having it up and running on your local machine for development and testing purposes.

### Prerequisites

_**Note:** if you've previously installed Gulp globally, run `npm rm --global gulp` to remove it. [Details here.](https://medium.com/gulpjs/gulp-sips-command-line-interface-e53411d4467)_

Make sure these are installed first.

- [Node.js](http://nodejs.org)
- [Gulp Command Line Utility][gulp] `npm install --global gulp-cli`

### Quick Start

1. Clone the repository
2. In bash/terminal/command line, `cd` into the project directory.
3. Run `npm install` to install required files and dependencies.
4. When it's done installing, run one of the tasks to get going:
   - `npm run build` to compile files.
   - `npm start` to serve and watch files with BrowserSync.
   - `npm test` run [TestCafe][test-cafe] static page tests.
5. Alternatively run:
   - `gulp` manually compiles files.
   - `gulp serve` automatically compiles files and applies changes using [BrowserSync](https://browsersync.io/) when you make changes to your source files.

## Built With

- [generator-njk-site][gen-njk] -- static site starter project
- [Nunjucks][nunjucks] -- template engine
- [Bulma][bulma] -- used for layout and styling
- [Gulp][gulp] -- automating the development wokflow
- [TestCafe][test-cafe] -- static web site end-to-end testing
- [GitLab Pages][pages] -- hosting the static website

## :page_with_curl: License

This project is licensed under the MIT License -- see the [LICENSE](LICENSE) file for details  

[nunjucks]: https://mozilla.github.io/nunjucks/
[gen-njk]: https://www.npmjs.com/package/generator-njk-site
[bulma]: https://bulma.io/
[gulp]: http://gulpjs.com
[test-cafe]: https://devexpress.github.io/testcafe/
[pages]: https://about.gitlab.com/stages-devops-lifecycle/pages/
