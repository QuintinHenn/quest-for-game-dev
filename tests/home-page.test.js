import { Selector } from 'testcafe';

fixture `Home Page Test`
  .page `http://localhost:8080/index.html`;

test('Q4GD: home page test', async test => {
  const header = await Selector('#page-header');
  const content = await Selector('#page-content');
  const footer = await Selector('#page-footer');

  await test
    .expect(header.exists && header.visible).eql(true)
    .expect(content.exists && content.visible).eql(true)
    .expect(footer.exists && footer.visible).eql(true);
});
