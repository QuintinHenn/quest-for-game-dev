import { Selector } from 'testcafe';

fixture `Resource Page Test`
  .page `http://localhost:8080/resources.html`;

test('Q4GD: game dev resources page test', async test => {
  const header = await Selector('#page-header');
  const content = await Selector('#page-content');
  const footer = await Selector('#page-footer');

  await test
    .expect(header.exists && header.visible).eql(true)
    .expect(content.exists && content.visible).eql(true)
    .expect(footer.exists && footer.visible).eql(true);
});
